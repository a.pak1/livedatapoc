package sample.platform_specific

interface PlatformProvider {
    val platformName: String
}
