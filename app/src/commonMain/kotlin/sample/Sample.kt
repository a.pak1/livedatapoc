package sample

import sample.platform_specific.MutableObservable
import sample.platform_specific.Observable
import sample.platform_specific.PlatformProvider
import sample.platform_specific.ViewModel

class SampleViewModel(private val platformProvider: PlatformProvider) : ViewModel() {

    private val _observable = MutableObservable<String>()
        .apply { newValue("Hello from shared module on ${platformProvider.platformName}") }

    val observable: Observable<String> = _observable
}